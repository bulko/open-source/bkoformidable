<?php
/**
 * SpamShield
 *
 * @package GuildPanel
 * @subpackage class
 * @author Golga for Oyana under GNU
 */
class SpamShield
{
	private $configRequest;
	private $configField;
	private $configFile;
	private $errors;

	public function __construct( Array $configRequest = [], Array $configField = [], Array $configFile = [] )
	{
		$this->configRequest = $this->mConfig( [
			'requestType' => 'post',
			'callBackType' => 'json',
			'shouldHave' => [
				'haveHoney' => false,// array of honey pot name & val
				'haveCSRF' => false, // array of CSRF Token name & val
				'haveTrashMailFilter' => false,// array of email var
				'haveOrigineCheck' => false,// Array of valid orgine
				'haveImputValidation' => false,// Array of imput rules
				'haveValidIp' => false,// No param
				'haveXmlHttprequest' => false,// No param
			],
		], $configRequest );
		$this->configField = $this->mConfig( [
			'shouldBe' => [
				'isString'
			],
			'shouldNotBe' => [
				'isRussian',
				'isChinese',
				'isJapanese',
				'isHtml',
				'isBBCode',
			],
			'isReq' => true,
		], $configField );
	}
	public function __destruct()
	{
		# code...
	}

	/*
	 * Language
	 */
	public function isRussian( String $str ): Bool
	{
		return preg_match( '/[А-Яа-яЁё]/u', $str );
	}
	public function isChinese( String $str ): Bool
	{
		return preg_match( '/\p{Han}+/u', $str );
	}
	public function isKanji( String $str ): Bool
	{
		return preg_match( '/[\x{4E00}-\x{9FBF}]/u', $str ) > 0;
	}
	public function isHiragana( String $str ): Bool
	{
		return preg_match( '/[\x{3040}-\x{309F}]/u', $str ) > 0;
	}
	public function isKatakana( String $str ): Bool
	{
		return preg_match( '/[\x{30A0}-\x{30FF}]/u', $str ) > 0;
	}
	public function isJapanese( String $str ): Bool
	{
		return $this->isKanji( $str ) || $this->isHiragana( $str ) || $this->isKatakana( $str );
	}
	public function isMultipleBypte( String $str ): Bool
	{
		return ( strlen( $str ) !== strlen( utf8_decode( $str ) ) );
	}

	/*
	 * Code
	 */
	public function isHtml( String $str ): Bool
	{
		return ( $str !== strip_tags( $str ) );
	}
	public function isBBCode( String $str ): Bool
	{
		$haveBBCode = false;
		$bbcodes = [ '[b]', '[/b]', '[i]', '[/i]', '[u]', '[/u]', '[s]', '[/s]', '[/size]', '[/style]', '[/color]', '[/style]', '[center]', '[/center]', '[left]', '[/left]', '[right]', '[/right]', '[quote]', '[/quote]', '[quote=]', '[/quote]', '[url]', '[/url]', '[url=', '[/url]', '[img]', '[img=', '[/img]', '[li]', '[/li]', '[*]', '[code]', '[/code]', '[code]', '[/code]', '[table]', '[/table]', '[tr]', '[/tr]', '[th]', '[/th]', '[td]', '[/td]', '[youtube]', '[/youtube]', ];
		foreach ( $bbcodes as $key => $code )
		{
			if ( strrpos( $str, $code ) )
			{
				$haveBBCode = true;
				break;
			}
		}
		return $haveBBCode;
	}

	/*
	 * Valid format
	 */
	public function isTrashMail( String $str ): Bool
	{
		$isTrash = false;
		$trashDomains = [ '0815.ru0clickemail.com', '0-mail.com', '0wnd.net', '0wnd.org', '10minutemail.com', '20minutemail.com', '2prong.com', '3d-painting.com', '4warding.com', '4warding.net', '4warding.org', '9ox.net', 'a-bc.net', 'ag.us.to', 'amilegit.com', 'anonbox.net', 'anonymbox.com', 'antichef.com', 'antichef.net', 'antispam.de', 'baxomale.ht.cx', 'beefmilk.com', 'binkmail.com', 'bio-muesli.net', 'bobmail.info', 'bodhi.lawlita.com', 'bofthew.com', 'brefmail.com', 'bsnow.net', 'bugmenot.com', 'bumpymail.com', 'casualdx.com', 'chogmail.com', 'cool.fr.nf', 'correo.blogos.net', 'cosmorph.com', 'courriel.fr.nf', 'courrieltemporaire.com', 'curryworld.de', 'cust.in', 'dacoolest.com', 'dandikmail.com', 'deadaddress.com', 'despam.it', 'despam.it', 'devnullmail.com', 'dfgh.net', 'digitalsanctuary.com', 'discardmail.com', 'discardmail.de', 'disposableaddress.com', 'disposeamail.com', 'disposemail.com', 'dispostable.com', 'dm.w3internet.co.ukexample.com', 'dodgeit.com', 'dodgit.com', 'dodgit.org', 'dontreg.com', 'dontsendmespam.de', 'dump-email.info', 'dumpyemail.com', 'e4ward.com', 'email60.com', 'emailias.com', 'emailias.com', 'emailinfive.com', 'emailmiser.com', 'emailtemporario.com.br', 'emailwarden.com', 'enterto.com', 'ephemail.net', 'explodemail.com', 'fakeinbox.com', 'fakeinformation.com', 'fansworldwide.de', 'fastacura.com', 'filzmail.com', 'fixmail.tk', 'fizmail.com', 'frapmail.com', 'garliclife.com', 'gelitik.in', 'get1mail.com', 'getonemail.com', 'getonemail.net', 'girlsundertheinfluence.com', 'gishpuppy.com', 'goemailgo.com', 'great-host.in', 'greensloth.com', 'greensloth.com', 'gsrv.co.uk', 'guerillamail.biz', 'guerillamail.com', 'guerillamail.net', 'guerillamail.org', 'guerrillamail.biz', 'guerrillamail.com', 'guerrillamail.de', 'guerrillamail.net', 'guerrillamail.org', 'guerrillamailblock.com', 'haltospam.com', 'hidzz.com', 'hotpop.com', 'ieatspam.eu', 'ieatspam.info', 'ihateyoualot.info', 'imails.info', 'inboxclean.com', 'inboxclean.org', 'incognitomail.com', 'incognitomail.net', 'ipoo.org', 'irish2me.com', 'jetable.com', 'jetable.fr.nf', 'jetable.net', 'jetable.org', 'jnxjn.com', 'junk1e.com', 'kasmail.com', 'kaspop.com', 'klzlk.com', 'kulturbetrieb.info', 'kurzepost.de', 'kurzepost.de', 'lifebyfood.com', 'link2mail.net', 'litedrop.com', 'lookugly.com', 'lopl.co.cc', 'lr78.com', 'maboard.com', 'mail.by', 'mail.mezimages.net', 'mail4trash.com', 'mailbidon.com', 'mailcatch.com', 'maileater.com', 'mailexpire.com', 'mailin8r.com', 'mailinator.com', 'mailinator.net', 'mailinator2.com', 'mailincubator.com', 'mailme.lv', 'mailmetrash.com', 'mailmoat.com', 'mailnator.com', 'mailnull.com', 'mailzilla.org', 'mbx.cc', 'mega.zik.dj', 'meltmail.com', 'mierdamail.com', 'mintemail.com', 'mjukglass.nu', 'mobi.web.id', 'moburl.com', 'moncourrier.fr.nf', 'monemail.fr.nf', 'monmail.fr.nf', 'mt2009.com', 'mx0.wwwnew.eu', 'mycleaninbox.net', 'myspamless.com', 'mytempemail.com', 'mytrashmail.com', 'netmails.net', 'neverbox.com', 'no-spam.ws', 'nobulk.com', 'noclickemail.com', 'nogmailspam.info', 'nomail.xl.cx', 'nomail2me.com', 'nospam.ze.tc', 'nospam4.us', 'nospamfor.us', 'nowmymail.com', 'objectmail.com', 'obobbo.com', 'odaymail.com', 'onewaymail.com', 'ordinaryamerican.net', 'owlpic.com', 'pookmail.com', 'privymail.de', 'proxymail.eu', 'punkass.com', 'putthisinyourspamdatabase.com', 'quickinbox.com', 'rcpt.at', 'recode.me', 'recursor.net', 'regbypass.comsafe-mail.net', 'safetymail.info', 'sandelf.de', 'saynotospams.com', 'selfdestructingmail.com', 'sendspamhere.com', 'sharklasers.com', 'shieldedmail.com', 'shiftmail.com', 'skeefmail.com', 'slopsbox.com', 'slushmail.com', 'smaakt.naar.gravel', 'smellfear.com', 'snakemail.com', 'sneakemail.com', 'sofort-mail.de', 'sogetthis.com', 'soodonims.com', 'spam.la', 'spamavert.com', 'spambob.net', 'spambob.org', 'spambog.com', 'spambog.de', 'spambog.ru', 'spambox.info', 'spambox.us', 'spamcannon.com', 'spamcannon.net', 'spamcero.com', 'spamcorptastic.com', 'spamcowboy.com', 'spamcowboy.net', 'spamcowboy.org', 'spamday.com', 'spamex.com', 'spamfree.eu', 'spamfree24.com', 'spamfree24.de', 'spamfree24.eu', 'spamfree24.info', 'spamfree24.net', 'spamfree24.org', 'spamgourmet.com', 'spamgourmet.net', 'spamgourmet.org', 'spamherelots.com', 'spamhereplease.com', 'spamhole.com', 'spamify.com', 'spaminator.de', 'spamkill.info', 'spaml.com', 'spaml.de', 'spammotel.com', 'spamobox.com', 'spamspot.com', 'spamthis.co.uk', 'spamthisplease.com', 'speed.1s.fr', 'suremail.info', 'tempalias.com', 'tempe-mail.com', 'tempemail.biz', 'tempemail.com', 'tempemail.net', 'tempinbox.co.uk', 'tempinbox.com', 'tempomail.fr', 'temporaryemail.net', 'temporaryinbox.com', 'tempymail.com', 'thankyou2010.com', 'thisisnotmyrealemail.com', 'throwawayemailaddress.com', 'tilien.com', 'tmailinator.com', 'tradermail.info', 'trash-amil.com', 'trash-mail.at', 'trash-mail.com', 'trash-mail.de', 'trash2009.com', 'trashmail.at', 'trashmail.com', 'trashmail.me', 'trashmail.net', 'trashmailer.com', 'trashymail.com', 'trashymail.net', 'trillianpro.com', 'tyldd.com', 'uggsrock.com', 'wegwerfmail.de', 'wegwerfmail.net', 'wegwerfmail.org', 'wh4f.org', 'whyspam.me', 'willselfdestruct.com', 'winemaven.info', 'wronghead.com', 'wuzupmail.net', 'xoxy.net', 'yogamaven.com', 'yopmail.com', 'yopmail.fr', 'yopmail.net', 'yuurok.com', 'zippymail.info', 'zoemail.com', ];
		foreach ( $trashDomains as $key => $trashDomain )
		{
			if ( strrpos( $str, '@' . $trashDomain ) )
			{
				$isTrash = true;
				break;
			}
		}
		return $isTrash;
	}
	public function isEmail( String $str ): Bool
	{
		return filter_var( $str, FILTER_VALIDATE_EMAIL );
	}
	public function isSiret( String $str ): Bool
	{
		if ( strlen( $str ) !== 14 )
		{
			return false;
		}
		$sum = 0;
		for ( $i = 0; $i !== 14; $i++ )
		{
			$tmp = ((($i + 1) % 2) + 1) * intval($str[$i]);
			if ($tmp >= 10)
			{
				$tmp -= 9;
			}
			$sum += $tmp;
		}
		return ( $sum % 10 === 0 );
	}
	public function isUrl( String $str ): Bool
	{
		return filter_var( $url, FILTER_VALIDATE_URL );
	}
	public function isConfigName( String $str ): Bool
	{
		return preg_match( '/^[a-zA-Z_0-9-]+$/', $str );
	}
	public function isPhpDateFormat( $str ): Bool
	{
		return preg_match( '/^[^<>]+$/', $str );
	}
	public function isDateFormat( String $str ): Bool
	{
		return preg_match('/^([0-9]{4})-((0?[0-9])|(1[0-2]))-((0?[0-9])|([1-2][0-9])|(3[01]))( [0-9]{2}:[0-9]{2}:[0-9]{2})?$/', $str);
	}
	public function isDate( String $str ): Bool
	{
		if (!preg_match('/^([0-9]{4})-((?:0?[0-9])|(?:1[0-2]))-((?:0?[0-9])|(?:[1-2][0-9])|(?:3[01]))( [0-9]{2}:[0-9]{2}:[0-9]{2})?$/', $str, $matches)) {
			return false;
		}
		return checkdate( (int) $matches[2], (int) $matches[3], (int) $matches[1] );
	}
	public function isBirthDate( String $str ): Bool
	{
		if (preg_match('/^([0-9]{4})-((?:0?[1-9])|(?:1[0-2]))-((?:0?[1-9])|(?:[1-2][0-9])|(?:3[01]))([0-9]{2}:[0-9]{2}:[0-9]{2})?$/', $str, $birth_date)) {
			if ($birth_date[1] > date('Y') && $birth_date[2] > date('m') && $birth_date[3] > date('d')
				|| $birth_date[1] == date('Y') && $birth_date[2] == date('m') && $birth_date[3] > date('d')
				|| $birth_date[1] == date('Y') && $birth_date[2] > date('m')) {
				return false;
			}
			return true;
		}
		return false;
	}
	public function isPhoneNumber( String $str ): Bool
	{
		return preg_match('/^[+0-9. ()-]*$/', $str);
	}
	public function isEan13( String $str ): Bool
	{
		return preg_match('/^[0-9]{0,13}$/', $str);
	}
	public function isUpc( String $str ): Bool
	{
		return preg_match('/^[0-9]{0,12}$/', $str);
	}
	public function isPostCode( String $str ): Bool
	{
		return preg_match('/^[a-zA-Z 0-9-]+$/', $str);
	}
	public function isZipCodeFormat( String $str ): Bool
	{
		return preg_match('/^[NLCnlc 0-9-]+$/', $str);
	}
	public function isAddress( String $str ): Bool
	{
		return preg_match(Tools::cleanNonUnicodeSupport('/^[^!<>?=+@{}_$%]*$/u'), $str);
	}
	public function isCityName( String $str ): Bool
	{
		return preg_match(Tools::cleanNonUnicodeSupport('/^[^!<>;?=+@#"°{}_$%]*$/u'), $str);
	}
	public function isValidSearch( String $str ): Bool
	{
		return preg_match(Tools::cleanNonUnicodeSupport('/^[^<>;=#{}]{0,64}$/u'), $str);
	}
	public function isMd5( String $str ): Bool
	{
		return preg_match('/^[a-f0-9A-F]{32}$/', $str);
	}
	public function isSha1( Strin $str ): Bool
	{
		return preg_match('/^[a-fA-F0-9]{40}$/', $str);
	}

	/*
	 * Valid type
	 */
	public function isFloat( $float ): Bool
	{
		return strval( (float) $float ) === strval( $float );
	}
	public function isUnsignedFloat($float): Bool
	{
		return strval( (float) $float ) === strval( $float ) && $float >= 0;
	}
	public function isString( $str ): Bool
	{
		return is_string( $str );
	}
		public function isInt( $int ): Bool
	{
		return ( (string) (int)$int === (string) $int || $int === false );
	}
	public function isUnsignedInt( $int ): Bool
	{
		return ( (string) (int) $int === (string) $int && $int < 4294967296 && $int >= 0 );
	}
	public function isBool($bool): Bool
	{
		return $bool === null || is_bool($bool) || preg_match('/^(0|1)$/', $bool);
	}
	public function getIp(): String
	{
	// check for shared internet/ISP IP
		if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) && validate_ip( $_SERVER['HTTP_CLIENT_IP'] ) )
		{
			return $_SERVER['HTTP_CLIENT_IP'];
		}
		// check for IPs passing through proxies
		if ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
		{
			// check if multiple ips exist in var
			if ( strpos( $_SERVER['HTTP_X_FORWARDED_FOR'], ',' ) !== false )
			{
				$iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				foreach ( $iplist as $ip )
				{
					if ( validate_ip( $ip ) )
					{
						return $ip;
					}
				}
			}
			else
			{
				if ( validate_ip( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
				{
					return $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
			}
		}
		if ( !empty( $_SERVER['HTTP_X_FORWARDED'] ) && validate_ip( $_SERVER['HTTP_X_FORWARDED'] ) )
		{
			return $_SERVER['HTTP_X_FORWARDED'];
		}
		if ( !empty( $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'] ) && validate_ip( $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'] ) )
		{
			return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
		}
		if ( !empty( $_SERVER['HTTP_FORWARDED_FOR'] ) && validate_ip( $_SERVER['HTTP_FORWARDED_FOR'] ) )
		{
			return $_SERVER['HTTP_FORWARDED_FOR'];
		}
		if ( !empty( $_SERVER['HTTP_FORWARDED'] ) && validate_ip($_SERVER['HTTP_FORWARDED'] ) )
		{
			return $_SERVER['HTTP_FORWARDED'];
		}

		// return unreliable ip since all else failed
		return $_SERVER['REMOTE_ADDR'];
	}
	public function isIp( String $ip ): Bool
	{
		if (strtolower( $ip ) === 'unknown')
		{
			return false;
		}
		// generate ipv4 network address
		$ip = ip2long( $ip );
		// if the ip is set and not equivalent to 255.255.255.255
		if ($ip !== false && $ip !== -1) {
			// make sure to get unsigned long representation of ip
			// due to discrepancies between 32 and 64 bit OSes and
			// signed numbers (ints default to signed in PHP)
			$ip = sprintf('%u', $ip);
			// do private network range checking
			if (
				( $ip >= 0 && $ip <= 50331647 )
				|| ( $ip >= 167772160 && $ip <= 184549375 )
				|| ( $ip >= 2130706432 && $ip <= 2147483647 )
				|| ( $ip >= 2851995648 && $ip <= 2852061183 )
				|| ( $ip >= 2886729728 && $ip <= 2887778303 )
				|| ( $ip >= 3221225984 && $ip <= 3221226239 )
				|| ( $ip >= 3232235520 && $ip <= 3232301055 )
				|| ( $ip >= 4294967040)
			)
			{
				return false;
			}
		}
		return true;
	}

	/*
	 * Valid Context
	 */
	public function haveHoney( Array $param ): Bool
	{
		$data = $this->getRequest();
		$valid = true;
		foreach ( $param as $k => $v )
		{
			if (
				!isset( $data[ $k ] )
				|| $data[ $k ] != $v // you should not set strict here cause the field could be empty
			)
			{
				return false;
			}
		}
		return true;
	}
	public function haveCSRF( Array $param ): Bool
	{
		return $this->haveHoney();
	}
	public function haveTrashMailFilter( Array $param ): Bool
	{
		$data = $this->getRequest();
		foreach ( $param as $v )
		{
			if (
				!isset( $data[ $v ] )
				|| !$this->isString( $data[ $v ] )
				|| !$this->isEmail( $data[ $v ] )
				|| $this->isTrashMail( $data[ $v ] )
			)
			{
				return false;
			}
		}
		return true;
	}
	public function haveOrigineCheck( Array $param ): Bool
	{
		$data = $this->getRequest();
		foreach ( $param as $v )
		{
			if (
				(
					isset( $_SERVER['HTTP_REFERER'] )
					&& strpos( $_SERVER['HTTP_REFERER'], $v ) !== 0
				)
				||
				(
					isset( $_SERVER['HTTP_ORIGIN'] )
					&& strpos( $_SERVER['HTTP_ORIGIN'], $v ) !== 0
				)
			)
			{
				return false;
			}
		}
		return true;
	}
	public function haveImputValidation( Array $param ): Bool
	{
		$data = $this->getRequest();
		return true;
	}
	public function haveValidIp( $param = [] ): Bool
	{
		return $this->isIp( $this->getIp() );
	}
	public function haveXmlHttprequest( $param = [] ): Bool
	{
		return ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) === 'xmlhttprequest' );
	}

	/*
	 * Helper
	 */
	public function cleanNonUnicodeSupport( String $pattern ): String
	{
		if ( !defined('PREG_BAD_UTF8_OFFSET') )
		{
			return $pattern;
		}
		return preg_replace( '/\\\[px]\{[a-z]{1,2}\}|(\/[a-z]*)u([a-z]*)$/i', '$1$2', $pattern );
	}

	public function isValidRequest( Array $config = [] )
	{
		$config = $this->mConfig( $this->configRequest, $config );
		foreach ( $config['shouldHave'] as $having => $param )
		{
			if ( $param !== false && !$this->$having( $param ) )
			{
				$this->addError( 'Invalid context... Are you a robot?'. $having );
			}
		}
		return $this->callBack();
	}

	public function isValidField( $str = null, Array $config = [] )
	{
		$config = $this->mConfig( $this->configField, $config );
		if ( empty( $str ) && !$config['isReq'] )
		{
			return $this->callBack();
		}
		elseif ( empty( $str ) && $config['isReq'] )
		{
			$this->addError( 'Champ obligatoire vide' );
			return $this->callBack();
		}
		if( !empty( $config['shouldBe'] ) )
		{
			foreach ( $config['shouldBe'] as $key => $should )
			{
				if( !$this->$should( $str ) )
				{
					$this->addError( 'Invalide format (' . $should . ')' );
				}
			}
		}
		if( !empty( $config['shouldNotBe'] ) )
		{
			foreach ( $config['shouldNotBe'] as $key => $shouldNot )
			{
				if( $this->$shouldNot( $str ) )
				{
					$this->addError( 'Format intedit (' . $should . ')' );
				}
			}
		}
		return $this->callBack();
	}

	public function callBack()
	{
		if ( isset( $this->configRequest['callBackType'] ) && strtolower( $this->configRequest['callBackType'] ) === 'debug' )
		{
			if ( empty( $this->errors ) )
			{
				return '<hr />Certified Safe By SpamShield';
			}
			else
			{
				return '<hr />Unsafe <hr />'. json_encode( $this->errors );
			}
		}
		elseif ( isset( $this->configRequest['callBackType'] ) && strtolower( $this->configRequest['callBackType'] ) === 'json' )
		{
			if ( empty( $this->errors ) )
			{
				echo 1;
				return true;
			}
			else
			{
				echo json_encode( $this->errors );
				return false;
			}
		}
		elseif ( empty( $this->errors ) )
		{
			return true;
		}
		return false;
	}

	public function addError( String $str ): Void
	{
		if ( empty( $this->error ) || !in_array( $str, $this->error ) )
		{
			$this->errors[] = $str;
		}
	}

	public function mConfig( Array $default, Array $override ): Array
	{
		return array_merge( $default, $override );
	}

	public function getRequest()
	{
		if ( $this->configRequest['requestType'] === 'get' )
		{
			return $_GET;
		}
		if ( $this->configRequest['requestType'] === 'post' )
		{
			return $_POST;
		}
		throw new Exception("Error Processing Request", 1);
	}

	public function unsetErrors()
	{
		$this->errors = [];
	}
}
?>
