(function ($) {
	var ready = function()
	{
		$(".ninja").css({"visibility": "hidden", "height": 0});
		$( ".bkoformidable" ).on( "submit", function( e )
		{
			e.preventDefault();
			console.log(this);
			var $selfForm = $(this);
			var formData = new FormData();
			var txtDatas = $selfForm.serializeArray();
			var it = 0;
			var $fileInputs = $selfForm.find('input[type=file]');
			$fileInputs.each(function(){
				formData.append(
					$fileInputs[it].name,
					$fileInputs[it].files[0]
				);
				it ++;
			});
			$selfForm.find('.submitBkoformidable').attr("disabled", true);
			$(txtDatas).each(function(){
				formData.append(
					this.name,
					this.value
				);
			})
			$.ajax({
				url: bkoFormidable.ajax_url,
				type: 'POST',
				data: formData,
				cache: false,
				dataType: 'json',
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function( data )
				{
					var $selfForm = $('.bkoformidable[data-template="' + data.templateName + '"]');
					if($selfForm.find(".success")[0]) {
						$selfForm.find(".success").fadeIn();
					}
					else{
						$selfForm.prepend(
							'<div class="alert alert-success alert-dismissible fade show">'
							+ '<div class="title">Le formulaire a bien été envoyé</div>'
							+ '</div>'
						).css({"font-size": "19px"});
					}
					$selfForm.trigger("reset");
					$selfForm.find('.submitBkoformidable').removeAttr("disabled");
					console.log(data);
				},
				error: function ( data )
				{
					var $selfForm = $('.bkoformidable[data-template="' + data.templateName + '"]');
					console.log( "error bkoformidable_submitForm", data.responseJSON );
					var msg = '';
					$.each( data.responseJSON, function(i, item) {
						if ( !item )
						{
							switch ( i )
							{
								case 'email':
									msg += '<li>Un problème lié à l\'envoi du mail est survenu.</li>';
									break;
								case 'file':
									msg += '<li>Un problème lié à vos pieces jointes est survenu.</li>';
									break;
								case 'db':
									msg += '<li>Un problème lié à l\'embasement est survenu.</li>';
									break;
								default:
									msg += '<li>Un problème inconnu est survenu.</li>';
									break;
							}
						}
					});
					if ( msg !== '' )
					{
						msg = '<ul class="msg-list">' + msg + '</ul>';
					}
					$( data.responseJSON )
					$selfForm.prepend(
						'<div class="alert alert-warning alert-dismissible fade show">'
						+ '<div class="title">Une ou plusieurs erreurs sont survenues</div>'
						+ msg
						+ '</div>'
					);
					$selfForm.find('.submitBkoformidable').removeAttr("disabled");
				}
			});
		});
	}
	document.addEventListener("turbolinks:load", ready);
})(jQuery);

