<?php
/*
Plugin Name: BkoFormidable
Depends:
Provides: BkoFormidable
Plugin URI:
Description: Options de formulaire avancé. Nécéssite le module Turbolinks.
Version: 3.0.0
Author: Bulko
Author URI: http://www.bulko.net/
License: http://www.wtfpl.net/
*/
if ( !function_exists( 'add_action' ) )
{
	wp_die( 'Hi there!  I\'m just a plugin, not much I can do when called directly.' );
}
define( '_BKOFORMIDABLE_FILE_', __FILE__ );
require_once( ABSPATH . '/wp-config.php' );
require_once(  plugin_dir_path( _BKOFORMIDABLE_FILE_ ) . 'init/ini.php' );
require_once(  plugin_dir_path( _BKOFORMIDABLE_FILE_ ) . 'init/shortCode.php' );
require_once(  plugin_dir_path( _BKOFORMIDABLE_FILE_ ) . 'admin/process.php' );
require_once(  plugin_dir_path( _BKOFORMIDABLE_FILE_ ) . 'admin/admin.php' );

