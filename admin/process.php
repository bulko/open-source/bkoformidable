<?php
include plugin_dir_path(_BKOFORMIDABLE_FILE_).'class/SpamShield.php';

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-16 )
 */
function bkoformidable_submitForm(): Void
{
	$data = $_POST;
	$callBack['templateName'] = $data['templateName'];
	$errors = [];
	$subject = '';

	$is_spam = false;
	$SPAMShieldReq = true;
	$SShield = new SpamShield([
		'requestType' => 'post',
		'callBackType' => 'bool',
		'shouldHave' => [
			'haveHoney' => ['honeyyyyyyyyyy'=>'', 'dk'=>sha1(date('Y-d.d-M') . '🦄' )], //CSRF Token
			'haveTrashMailFilter' => ["mail"],// bool
			'haveOrigineCheck' => false,// Array of valid origine
			'haveImputValidation' => false,// bool
			'haveValidIp' => [],// bool
			'haveXmlHttprequest' => [],// bool
		],
	]);
	$SShield->unsetErrors();

	//Change this condition according to the template file (on all the required fields) that you are using. See Class/SpamShield.php.
	if
	(
		!$SShield->isValidField( $_POST["nom"], [
			'shouldBe' => [
				'isString',
			],
		] )

		|| !$SShield->isValidField( $_POST["télephone"], [
			'shouldBe' => [
				'isString',
				'isPhoneNumber',
			],
		] )

		|| !$SShield->isValidField( $_POST["email"], [
			'shouldBe' => [
				'isString',
				'isEmail',
			],
			'shouldNotBe' => [
				'isTrashMail',
			],
		] )

		|| !$SShield->isValidField( $_POST["message"], [
			'shouldBe' => [
				'isString',
			],
			'shouldNotBe' => [
				'isRussian',
				'isChinese',
				'isJapanese',
				'isHtml',
				'isBBCode',
			],
			'isReq' => false,
		] )
		|| !$SShield->getIp()
	)
	{
		$is_spam = true;
		$SPAMShieldReq = false;
	}

	if ( $SPAMShieldReq === true )
	{
		$callBack['valid'] = true;
		$callBack['file'] = bkoforminable_saveFile( $data );
		$callBack['db'] = bkoforminable_saveForm( $data , 0);
		$callBack['email'] = bkoforminable_sendMail( $data );
	}
	elseif ($is_spam === true){
		$callBack['valid'] = true;
		$callBack['file'] = bkoforminable_saveFile( $data );
		$callBack['db'] = bkoforminable_saveForm( $data , 1);
	}
	else
	{
		$callBack['valid'] = false;
		$callBack['file'] = false;
		$callBack['db'] = false;
		$callBack['email'] = false;
	}

	if($callBack['valid'] === false){
		header("HTTP/1.1 403 Forbidden");
	}
	else{
		foreach ( $callBack as $key => $valid )
		{
			if( !$valid )
			{
				header("HTTP/1.1 400 Bad request");
			}
		}
	}

	echo json_encode( $callBack );
	die();
}
/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-18 )
 */
function bkoforminable_submitCommentForm(): Void
{
	$data = $_GET;
	$callBack["db"] = bkoforminable_saveForm( $data , 0);
	echo json_encode( $callBack );
	die();
}
/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-18 )
 */
function bkoforminable_saveFile( Array &$data = [] ): Bool
{
	if( !empty( $_FILES ) )
	{
		if ( ! function_exists( 'wp_handle_upload' ) )
		{
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		foreach ( $_FILES as $key => $uploadedfile)
		{
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
			if ( $movefile && ! isset( $movefile['error'] ) )
			{
				$data[ $key ] = $movefile['url'];
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-17 )
 */
function bkoforminable_cleanData( Array &$data = [] ): Array
{
	$unwantedDatas = [
		'templateName',
		'honeyyyyyyyyyy',
		'successMessage',
	];
	foreach ( $unwantedDatas as $unwantedData )
	{
		if ( isset( $data[ $unwantedData] ) )
		{
			unset( $data[ $unwantedData] );
		}
	}
	return $data;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-17 )
 */
function bkoforminable_saveForm( Array &$data = [] , Int $spam): Bool
{
	global $wpdb;
	$table_name = bkoforminable_get_table_name();
	$tpl = $data['templateName'];
	if ( isset( $data['id'] ) )
	{
		$sql = "UPDATE  $table_name SET" .
		"`comment` = '" . $data['comment'] . "'" .
		"WHERE `id` = " . $data['id'] . ";";
	}
	else
	{
		bkoforminable_cleanData( $data );
		$sql = "INSERT INTO $table_name (" .
		"`template_name`," .
		"`url`," .
		"`content`," .
		"`created_at`," .
		"`is_spam`)" .
		"VALUES (" .
			"'" . $tpl . "'," .
			"'" . $_SERVER["HTTP_REFERER"] . "'," .
			"'" . json_encode( $data, JSON_UNESCAPED_UNICODE ) . "'," .
			"'" . date( 'Y-m-d h:i:s', time() ) . "'," .
			"" . $spam . "" .
		");";
	}
	return $wpdb->query( $sql );
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-17 )
 */
function bkoforminable_sendMail( Array &$data = [] ): Bool
{
	$mailTo = explode( ",", _BKOFORMIDABLE_TO_ );
	if ( isset( $mailTo[0] ) )
	{
		$to = $mailTo[0];
		$subject = _BKOFORMIDABLE_SUBJECT_;
		$body = '';
		foreach ( $data as $key => $value )
		{
			$body .= '<p><strong>' . $key . '</strong> ' . $value . '</p>';
		}
		$headers = [
			'From: ' . $mailTo[0],
			'Content-Type: text/html; charset=UTF-8',
		];
		for ( $i=1; $i < sizeof( $mailTo ); $i++ )
		{
			$headers[] = 'Cc: ' . $mailTo[$i];
		}
		return wp_mail( $to, $subject, $body, $headers, [] );
	}
	return false;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since AA 0.1 (11/07/2016 219de3a2e59b09fb8f5954b609ea44167536a7d9)
 * @see NHAM Controller.php
 */
function getCurlData( String $url, Array $fields ): Array
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	return (array) json_decode( curl_exec($ch) );
}

add_action( 'wp_ajax_bkoformidable_submitForm',"bkoformidable_submitForm", 10 );
add_action( 'wp_ajax_nopriv_bkoformidable_submitForm',"bkoformidable_submitForm", 10 );
add_action( 'wp_ajax_bkoforminable_submitCommentForm',"bkoforminable_submitCommentForm", 10 );
add_action( 'wp_ajax_nopriv_bkoforminable_submitCommentForm',"bkoforminable_submitCommentForm", 10 );
?>
