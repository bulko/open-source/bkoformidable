<?php

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 *
 * @return String
 */
function bkoforminable_adminHeader(): String
{
	$directories = glob( plugin_dir_path( _BKOFORMIDABLE_FILE_ ) . 'tpl/*' );
	$templates = [];
		$npTemplates = count( $directories );
	echo '<div class="wrap bkoforminable">';
	echo "<h1>" . _BKOFORMIDABLE_TITLE_ . "</h1>";
	if ( $npTemplates > 1 )
	{
		echo "<div class='wp-filter'>";
	}
	for ( $i = count( $directories ) -1; $i > -1; $i -- )
	{
		$expld =
			ucfirst(
				substr(
					end(
						explode( "/", $directories[ $i ] )
					)
					, 0, -5
				)
			);
		array_push( $templates, $expld );
		if ( $npTemplates > 0 && $_GET["tpl"] === $expld )
		{
			echo "<a class='button active elect-mode-toggle-button' title='" . $expld . "' href='?page=bkoforminableadmin.php&tpl=" . $expld . " ' >" . $expld . "</a>";
		}
		elseif ( $npTemplates > 0 )
		{
			echo "<a class='button elect-mode-toggle-button' title='" . $expld . "' href='?page=bkoforminableadmin.php&tpl=" . $expld . " ' >" . $expld . "</a>";
		}
	}
	echo "<a class='button elect-mode-toggle-button right' title='Configuration' href='?page=bkoforminableadmin.php&config' >Configuration</a>";
	if ( $npTemplates > 1 )
	{
		echo "</div>";
	}
	$activeTpl = $templates[0];
	if ( !empty( $_GET["tpl"] ) && in_array( $_GET["tpl"], $templates ) )
	{
		$activeTpl = $_GET["tpl"];
	}
	return $activeTpl;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 *
 * @param String $tplName
 * @return Array
 */
function bkoforminable_getData( String $tplName ): Array
{
	$is_spam = " AND `is_spam` = 0";
	if (isset($_GET['is_spam'])){
		$is_spam = '';
	}
	global $wpdb;
	$sql = "SELECT * FROM " .
	bkoforminable_get_table_name() .
	" WHERE `template_name` = '" . $tplName . "'" .
	" AND `deleted_at` IS NULL" .
	$is_spam .
	" ORDER BY `id` DESC" .
	";";
	return $wpdb->get_results( $sql );
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-17 )
 *
 * @param Mixed $data
 * @return Array
 */
function bkoforminable_decodes( String $data ): Array
{
	return (array) json_decode(
		trim(
			preg_replace(
				'/\s\s+|\_/',
				' ',
				nl2br( $data )
			)
		)
	);
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 *
 * @param array $data
 * @return Boolean
 */
function bkoforminable_displayTable( Array $data = [] ): Bool
{
	$tpl = '&tpl=' . $_GET['tpl'];
	if ( empty( $data ) || empty( $data[0] ) )
	{
		return false;
	}
	$is_spam = "&is_spam";
	$btn_spam = "Afficher les spams";
	if (isset($_GET['is_spam'])){
		$is_spam = '';
		$btn_spam = "Ne pas afficher les spams";
	}
	$size = sizeOf( $data ) - 1;
	$key = array_keys( (array) $data[ 0 ] );
	$headerContent = bkoforminable_decodes( $data[ 0 ]->content );
	$jsonKey = array_keys( $headerContent );
	echo "<div class='dataTableContainer'>";
	echo "<a href='' class='downloadCsv button'>Télécharger un CSV</a>";
	echo " <a href='' class='reloadCsv'>Régénérer le CSV (" . date_i18n( get_option( 'time_format' ), time() ) . ")</a>";
	echo "<a href='admin.php?page=bkoforminableadmin.php" . $tpl . $is_spam . "' class='displaySpam button'> " . $btn_spam . " </a>";
	echo "<table class='datatables'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th class='haveDropDown'> id </th>";
	echo "<th> date </th>";
	for ( $i = 0; $i < sizeOf( $jsonKey ) ; $i++)
	{
		echo "<th>" . $jsonKey[$i] . "</th>";
	}
	echo "<th> Commentaire </th>";
	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";
	for ( $i = $size; $i > -1 ; $i-- )
	{
		echo "<tr class='spam-".$data[$i]->is_spam."'>";
		echo "<td>" . $data[$i]->id . "</td>";
		echo "<td>" .  date_i18n( get_option( 'date_format' ), strtotime( $data[$i]->created_at ) ) . "</td>";
		$contentData = bkoforminable_decodes( $data[$i]->content );
		for ( $i2 = 0; $i2 < sizeOf( $jsonKey ) ; $i2++ )
		{
			echo "<td>" . $contentData[ $jsonKey[$i2] ] . "</td>";
		}
		echo "<td>" .
				"<div class='placeholder'>" .
					"<pre class='comment'>" .
						$data[$i]->comment .
					"</pre>" .
					"<span class='toggle-form'>✎</span>" .
				"</div>" .
				"<form class='form' method='post'>" .
					"<input type='hidden' name='id' value='" . $data[$i]->id . "' />" .
					"<textarea name='comment' class='comment-field'>" . $data[$i]->comment . "</textarea>" .
					"<input type='submit' class='submit' value='💾' />" .
				"</form>" .
			"</td>";
		echo "</tr>";
	}
	echo "</tbody>";
	echo "<tfoot>";
	echo "<tr>";
	echo "<th class='haveDropDown'> id </th>";
	echo "<th> date </th>";
	for ( $i = 0; $i < sizeOf( $jsonKey ) ; $i++)
	{
		echo "<th class='haveDropDown'>" . $jsonKey[$i] . "</th>";
	}
	echo "<th> Commentaire </th>";
	echo "</tr>";
	echo "</tfoot>";
	echo "</table>";
	echo "</div>";
	return true;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 *
 * @return Void
 */
function bkoforminable_displayFooter(): Void
{
	echo "<hr />";
	echo "</div>";
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 *
 * @return Void
 */
function bkoforminable_displayConfig(): Void
{
	echo '<div class="notice notice-info is-dismissible">';
		echo '<li>Certaines options avancées demandent la réinitialisation du module et des données.</li></ul>';
	echo '</div>';
	echo '<form method="post" action="options.php">';
		settings_fields( 'bkoformidable_options_group' );
		echo '<h3>Options des emails</h3>';
		echo '<p>';
			echo '<label>De </label>';
			echo '<input type="text" name="bkoformidable_mail_from" value="' . get_option('bkoformidable_mail_from') . '" />';
		echo '</p>';
		echo '<p>';
			echo '<label>Sujet </label>';
			echo '<input type="text" name="bkoformidable_mail_subject" value="' . get_option('bkoformidable_mail_subject') . '" />';
		echo '</p>';
		echo '<p>';
			echo '<label>Destinataire </label>';
			echo '<input type="text" name="bkoformidable_mail_to" value="' . get_option('bkoformidable_mail_to') . '" />';
			echo '<i> Si plusieurs destinataires, merci de les séparer par des virgules.</i>';
		echo '</p>';
		echo '<h3>Menu</h3>';
		echo '<a class="button" target="_blank" href="https://developer.wordpress.org/resource/dashicons/" title="Acceder à Dashicons">Accéder à Dashicons</a>';
		echo '<p>';
			echo '<label>Nom </label>';
			echo '<input type="text" name="bkoformidable_menu_name" value="' . get_option('bkoformidable_menu_name') . '" />';
		echo '</p>';
		echo '<p>';
			echo '<label>Icône </label>';
			echo '<input type="text" name="bkoformidable_menu_icone" value="' . get_option('bkoformidable_menu_icone') . '" />';
		echo '</p>';
		echo '<p>';
			echo '<label>Position </label>';
			echo '<input type="text" name="bkoformidable_menu_position" value="' . get_option('bkoformidable_menu_position') . '" />';
		echo '</p>';
		echo '<h3>Options avancées</h3>';
		echo '<a class="button" target="_blank" href="https://gitlab.com/bulko/open-source/bkoformidable/wikis/home" title="Acceder au Wiki">Accéder au Wiki</a>';
		echo '<p>';
			echo '<label>Titre </label>';
			echo '<input type="text" name="bkoformidable_title" value="' . get_option('bkoformidable_title') . '" />';
		echo '</p>';
		echo '<p>';
			echo '<label>Tag </label>';
			echo '<input type="text" name="bkoformidable_tag_name" value="' . get_option('bkoformidable_tag_name') . '" />';
			echo '<i> La customisation du tag doit être répliquée sur les différentes pages sur lesquelles il a été utilisé.</i>';
		echo '</p>';
		echo '<p>';
			echo '<label>Nom de la table </label>';
			echo '<input type="text" name="bkoformidable_table_name" value="' . get_option('bkoformidable_table_name') . '" />';
			echo '<i> Attention, la customisation de cette valeur demande la réinitialisation du plugin (et donc la perte des données).</i>';
		echo '</p>';
		submit_button();
	echo '</form>';
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2019-07-18 )
 *
 * @param String $activeTpl
 * @return Void
 */
function bkoforminable_displayTagNotice( String$activeTpl ): Void
{
	echo '<div class="notice notice-info is-dismissible">';
		echo 'Pour intégrer ce formulaire vous pouvez utiliser le tag ci-dessous dans n\'importe lequel de vos contenus.<br />';
		echo '<code>[' ._BKOFORMIDABLE_TAG_NAME_ . ' formName="' . $activeTpl . '"]</code>';
	echo '</div>';
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 *
 * @return Void
 */
function bkoforminable_admin(): Void
{
	if ( !current_user_can( 'bkoforminable' ) )
	{
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	$activeTpl = bkoforminable_adminHeader();
	if ( isset( $_GET["config"] ) )
	{
		bkoforminable_displayConfig();
	}
	else
	{
		$data = bkoforminable_getData( $activeTpl );
		bkoforminable_displayTagNotice( $activeTpl );
		bkoforminable_displayTable( $data );
	}
	bkoforminable_displayFooter();
}
