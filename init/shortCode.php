<?php
/**
 * Bkoformidabe
 *
 * short code generation
 * Param + Tpl + asset link (css & js)
 */

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-11 )
 */
function bkoFormidable_getTpl( String $name ): String
{
	$path = plugin_dir_path( _BKOFORMIDABLE_FILE_ ) . "tpl/" . $name . ".html";
	if ( file_exists( $path ) )
	{
		$form = '
		<form method="post" class="bkoformidable ' . $name . '" data-template="' . $name . '">
			<div class="ninja">
				<input type="hidden" name="action" value="bkoformidable_submitForm">
				<input type="hidden" name="templateName" value="' . $name . '">
				<label>
					nin nin
				</label>
				<input type="text" name="honeyyyyyyyyyy" value="" size="40" />
			</div>';
		$form .= file_get_contents( $path );
		$form .= '
			<input type="submit" class="submitBkoformidable" value="Envoyer" />
		</form>';
		return $form;
	}
	return "<hr />{Error form " . $name . " not found}<hr />";
}

/**
 * [bartag formName="foo-value"]
 *
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-11 )
 */
function bkoFormidable_tag( Array $atts ): String
{
	$a = shortcode_atts(
		[
			'formName' => $atts['formname'],
		],
	$atts );

	$data = bkoFormidable_getTpl( $a['formName'] );
	return $data;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 2.0.0 ( 2019-07-18 )
 */
function bkoFormidable_enque(): VOid
{
	wp_enqueue_script('bkoforminablejs', plugin_dir_url( _BKOFORMIDABLE_FILE_ ) . 'asset/front.js', ["jquery"] );
	wp_localize_script( 'bkoforminablejs', 'bkoFormidable',
		[
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'root' => site_url(),
			'home' => is_front_page(),
			'tagName' => _BKOFORMIDABLE_TAG_NAME_,
		]
	);
}

add_action( 'wp_enqueue_scripts', 'bkoFormidable_enque' );
add_shortcode( _BKOFORMIDABLE_TAG_NAME_, 'bkoFormidable_tag' );
