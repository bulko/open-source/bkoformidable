<?php
/**
 * Bkoformidabe
 *
 * Initialisation param
 * Set up data base & admin menu
 */

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 */
function bkoforminable_get_table_name(): String
{
	global $wpdb;
	return $wpdb->prefix . _BKOFORMIDABLE_TABLE_;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-11 )
 */
function bkoforminable_install(): Bool
{
	global $wpdb;
	global $bkoforminable_db_version;
	$bkoforminable_db_version = '1.3';
	$table_name = bkoforminable_get_table_name();
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
	`id` MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
	`template_name` TINYTEXT NULL COLLATE 'utf8mb4_unicode_520_ci',
	`url` VARCHAR(55) NULL DEFAULT '' COLLATE 'utf8mb4_unicode_520_ci',
	`content` TEXT NULL COLLATE 'utf8mb4_unicode_520_ci',
	`comment` TEXT NULL COLLATE 'utf8mb4_unicode_520_ci',
	`created_at` DATETIME NULL DEFAULT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	`deleted_at` DATETIME NULL DEFAULT NULL,
	`is_spam` BIT(1) NOT NULL DEFAULT 0 COMMENT '0 if not spam, 1 if spam',
	PRIMARY KEY (`id`)
) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	add_option( 'bkoforminable_db_version', $bkoforminable_db_version );
	return true;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-11 )
 */
function bkoforminable_uninstall(): Bool
{
	global $wpdb;
	$table_name = bkoforminable_get_table_name();
	$sql = "DROP TABLE $table_name;";
	$wpdb->query( $sql );
	delete_option( 'bkoforminable_db_version' );
	return true;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-11 )
 */
function bkoforminable_menu(): Bool
{
	add_menu_page(
		_BKOFORMIDABLE_MENU_NAME_,
		_BKOFORMIDABLE_MENU_NAME_,
		'bkoforminable',
		'bkoforminableadmin.php',
		'bkoforminable_admin',
		_BKOFORMIDABLE_MENU_ICONE_,
		_BKOFORMIDABLE_MENU_POSITION_
	);
	return true;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2018-07-13 )
 */
function bkoforminable_set_cap(): Bool
{
	global $wp_roles;
	// administrator
	$wp_roles->add_cap('administrator','bkoforminable');
	$wp_roles->add_cap('administrator','bkoforminable_export');
	$wp_roles->add_cap('administrator','bkoforminable_edit');
	$wp_roles->add_cap('administrator','bkoforminable_root');
	// editor
	$wp_roles->add_cap('editor','bkoforminable');
	$wp_roles->add_cap('editor','bkoforminable_export');
	$wp_roles->add_cap('editor','bkoforminable_edit');
	$wp_roles->add_cap('editor','bkoforminable_root');
	// author
	$wp_roles->add_cap('author','bkoforminable');
	$wp_roles->add_cap('author','bkoforminable_export');
	$wp_roles->add_cap('author','bkoforminable_edit');
	// contributor
	$wp_roles->add_cap('contributor','bkoforminable');
	$wp_roles->add_cap('contributor','bkoforminable_edit');
	return true;
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 2.0.0 ( 2019-07-18 )
 */
function bkoformidable_enqueue( String $hook ): Void
{
	if ('toplevel_page_bkoforminableadmin' !== $hook) {
		return;
	}
	wp_deregister_script('jquery');
	wp_enqueue_style('datatablescss', 'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css');
	wp_enqueue_style('bkoformidablecss', plugin_dir_url( _BKOFORMIDABLE_FILE_ ) . 'asset/admin.css');
	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.3.1.js' );
	wp_enqueue_script('datatablesjs', 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', ["jquery"]);
	wp_enqueue_script('bkoforminablejs', plugin_dir_url( _BKOFORMIDABLE_FILE_ ) . 'asset/admin.js', ["jquery"]);
}

/**
 * @author Golga <r-ro@bulko.net>
 * @since 1.0.0 ( 2019-07-18 )
 */
function bkoformidable_register_settings(): Void
{
	$opts = [
		'bkoformidable_tag_name' => 'bkoformidable',
		'bkoformidable_table_name' => 'bkoformidable',
		'bkoformidable_title' => 'BkoFormidable',
		'bkoformidable_mail_from' => 'bkoformidable',
		'bkoformidable_mail_subject' => 'Messages envoyé depuis le site',
		'bkoformidable_mail_to' => 'r-ro@bulko.net',
		'bkoformidable_menu_name' => 'BkoFormidable',
		'bkoformidable_menu_icone' => 'dashicons-format-status',
		'bkoformidable_menu_position' => '20',
	];
	foreach ( $opts as $opt => $optVal )
	{
		add_option( $opt, $optVal );
		register_setting( 'bkoformidable_options_group', $opt, 'bkoformidable_callback' );
	}
}

function bkoformidable_initConst(): Void
{
	define( '_BKOFORMIDABLE_TAG_NAME_', get_option( 'bkoformidable_tag_name' ) );
	define( '_BKOFORMIDABLE_TABLE_', get_option( 'bkoformidable_table_name' ) );
	define( '_BKOFORMIDABLE_TITLE_', get_option( 'bkoformidable_title' ) );
	define( '_BKOFORMIDABLE_FROM_', get_option( 'bkoformidable_mail_from' ) );
	define( '_BKOFORMIDABLE_SUBJECT_', get_option( 'bkoformidable_mail_subject' ) );
	define( '_BKOFORMIDABLE_TO_', get_option( 'bkoformidable_mail_to' ) );
	define( '_BKOFORMIDABLE_MENU_NAME_', get_option( 'bkoformidable_menu_name' ) );
	define( '_BKOFORMIDABLE_MENU_ICONE_', get_option( 'bkoformidable_menu_icone' ) );
	define( '_BKOFORMIDABLE_MENU_POSITION_', get_option( 'bkoformidable_menu_position' ) );
}

bkoformidable_initConst();
register_activation_hook( _BKOFORMIDABLE_FILE_, 'bkoforminable_install' );
register_deactivation_hook( _BKOFORMIDABLE_FILE_, 'bkoforminable_uninstall' );

if ( is_admin() )
{
	add_action( 'after_setup_theme', 'bkoforminable_set_cap' );
	add_action( 'admin_init', 'bkoformidable_register_settings' );
	add_action( 'admin_menu', 'bkoforminable_menu' );
	add_action( 'admin_enqueue_scripts', 'bkoformidable_enqueue' );
}
